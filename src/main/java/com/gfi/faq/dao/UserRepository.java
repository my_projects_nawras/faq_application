package com.gfi.faq.dao;

import com.gfi.faq.model.Question;
import com.gfi.faq.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
