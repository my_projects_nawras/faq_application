package com.gfi.faq.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN;

}
